package com.atm.modules;

import java.text.NumberFormat;
import java.util.Scanner;

public class TransactionHandler {
        //functions
        public void checkBalance(Accounts account){
            NumberFormat formatter = NumberFormat.getCurrencyInstance();
            String moneyString = formatter.format(account.getBalance());
            System.out.println("Your remaining balance is: "+moneyString);
        }

        public void withdraw(Accounts account){
            Scanner winput = new Scanner(System.in);
            System.out.println("Enter amount to withdraw(maximum 50000): ");
            if(!winput.hasNextFloat()){
                System.out.println("Invalid amount format!");
                withdraw(account);
            }else{
                float withdrawAmount = winput.nextFloat();
                account.withdraw(withdrawAmount,account.getAccountNum());
            }
        }

        public void deposit(Accounts account){
            Scanner dinput = new Scanner(System.in);
            System.out.println("Enter amount to deposit: ");
            if(!dinput.hasNextFloat()){
                System.out.println("Invalid amount format!");
                dinput.nextLine();
            }else{
                float depositAmount;
                depositAmount = dinput.nextFloat();
                account.deposit(depositAmount, account.getAccountNum());
            }
        }

    public boolean doAnotherTransaction(){
        Scanner rinput = new Scanner(System.in);
        while(true){
            System.out.println("Do you want another transaction? Type 'y'-yes or 'n' no");
            String response = rinput.next();
            if(response.charAt(0)=='y'){
                break;
            }
            else if (response.charAt(0)=='n') {

                System.out.println("Exiting program...");
                return true;
            }
            else{
                System.out.println("Invalid command");
                rinput.nextLine();
            }
        }
        return false;
    }
}
