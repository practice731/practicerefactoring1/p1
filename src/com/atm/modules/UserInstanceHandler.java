package com.atm.modules;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;

public class UserInstanceHandler {
        //get input accountnum
        private int acctNum;
        private int attempt = 1;
        private final AtomicReference<Boolean> loggedIn = new AtomicReference<>(false);
        private final AtomicReference<Accounts> loginAccount = new AtomicReference<>();
        private final Scanner input = new Scanner(System.in);

        public boolean getLoggedIn (){
            return loggedIn.get();
        }

        public Accounts getLoginAccount(){
            return loginAccount.get();
        }

        public void accountValidator(ArrayList<Accounts> accounts){
            //validate account number if wrong user is allowed 2 more attempts.
        loginLoop: while (attempt<=3){
            System.out.println("Enter Account number: ");
            boolean acctFormatCorrect = input.hasNextInt();
            if (!acctFormatCorrect){
//                throw new Exception("Invalid account format");
                System.out.println("Invalid account format");
                input.nextLine();
            }
            else {
                acctNum = input.nextInt();
                int finalAcctNum = acctNum;
                accounts.forEach((account) -> {
                    if (finalAcctNum == (account.getAccountNum())) {
                        System.out.println("----------------------------------------");
                        System.out.println("|Account No: " + finalAcctNum + " " + account.getName() + "|");
                        System.out.println("----------------------------------------");
                        loggedIn.set(true);
                        loginAccount.set(account);
                    }
                });

                //breaks the loop if logged in with valid account
                if (loggedIn.get()) {
                    break loginLoop;
                }

                //message changes depending on number of attempts
                if (attempt <= 2) {
                    System.out.println("Wrong account number!" + attempt + " attempt");

                    input.nextLine();
                } else {
                    System.out.println("Wrong account number!");
                    System.out.println("Reached maximum (3) attempts.");
                }
                attempt++;
            }
        }// end of login loop

        }
}
