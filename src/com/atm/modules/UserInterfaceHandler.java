package com.atm.modules;

public class UserInterfaceHandler {
    public void greetUser(){
        System.out.println("****************************");
        System.out.println("WELCOME TO MALACAÑANG BANK!");
        System.out.println("****************************");
    }

    public void userActionMenu(){
        System.out.println("What do you want to do?");
        System.out.println("Press [c] to check your balance. [w] to withdraw. [d] to deposit.");
        System.out.println("Type 'exit' to quit.");
    }
}
