package com.atm.modules;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;

public class ATMHandler{

    private final UserInstanceHandler userInstanceHandler = new UserInstanceHandler();
    private final UserInterfaceHandler userInterfaceHandler = new UserInterfaceHandler();
    private final TransactionHandler transactionHandler = new TransactionHandler();
    private final Scanner input = new Scanner(System.in);

    public void runATM(ArrayList<Accounts> accounts){

        userInstanceHandler.accountValidator(accounts);

        //MAIN PROGRAM UPON SUCCESSFUL LOGIN
        if (userInstanceHandler.getLoggedIn()){
            boolean exit = false;
            userInterfaceHandler.greetUser();

            //DO WHILE LOOP
            do {
                //Asks User what type of transaction to do
                userInterfaceHandler.userActionMenu();
                String action = input.next();

            if (action.charAt(0)=='c'){
                //Check Balance
                transactionHandler.checkBalance(userInstanceHandler.getLoginAccount());
                //Asks user if they want to do another Transaction
                exit = transactionHandler.doAnotherTransaction();

            } else if (action.charAt(0)=='w') {
                //Withrdaw
                transactionHandler.withdraw(userInstanceHandler.getLoginAccount());
                //Asks user if they want to do another Transaction
                exit = transactionHandler.doAnotherTransaction();

            } else if (action.charAt(0)=='d') {
                //Deposit
                transactionHandler.deposit(userInstanceHandler.getLoginAccount());
                //Asks user if they want to do another Transaction
                exit = transactionHandler.doAnotherTransaction();

            } else if (action.equals("exit")) {
                System.out.println("Exiting program...");
                exit = true;
            }
            else{
                System.out.println("Invalid command.");
            }
            } while (!exit);
//        }
        }//end of main program
    }

}
