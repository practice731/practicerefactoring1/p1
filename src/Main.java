import com.atm.modules.*;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;


public class Main {

    public static void main(String[] args) throws Exception {
        ATMHandler atmHandler = new ATMHandler();

        //Initialized accounts
        Accounts cauyan = new Accounts("Thomas Cyril Cauyan", 50000, 123456);
        Accounts mocha = new Accounts("Mocha Cauyan", 150000, 246810);

        ArrayList<Accounts> accounts = new ArrayList<>();
        accounts.add(cauyan);
        accounts.add(mocha);
        
        atmHandler.runATM(accounts);
    }
}